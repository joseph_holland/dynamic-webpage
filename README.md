# Dynamic Webpage

1. install using `pip install -r requirements.txt`
2. run using `flask --app app/app.py run --debug`
3. navigate to `127.0.0.1:5000/`
4. look through app.py and try navigating to the different routes, e.g. `127.0.0.1:5000/page2`
5. try use CURL to make a PUT request to `127.0.0.1:5000/data` and change some of the data (hint, the JSON object keys should be the same)
6. try extend the `data()` function to handle POST requests

See
- https://flask.palletsprojects.com/en/2.3.x/quickstart/#a-minimal-application
- https://blog.miguelgrinberg.com/post/dynamically-update-your-flask-web-pages-using-turbo-flask
- https://reqbin.com/req/c-d4os3720/curl-put-example
