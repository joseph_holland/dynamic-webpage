from flask import Flask, request, render_template

from turbo_flask import Turbo

# initialise the flask app
app = Flask(__name__)
# initialise turbo
turbo = Turbo(app)


config = {
    'name': 'Arthur Jackson',
    'num_of_sheds': 2,
    'something': 'Completely different'
}


# This is the root level of a web page, e.g. http://127.0.0.1:5000
@app.route("/")
def hello_world():
    print(data)
    # Render template renders html files. These html files are placed in a directory called templates.
    # data can be passed to the template by specifying an optional parameter (or several)
    # The optional parameter name can be anything. This is the name that is used in the template
    return render_template('home.html', config=config)

# this is a page that goes off root, e.g. http://127.0.0.1:5000/page2
@app.route("/page2")
def page2():
    # raw html or even raw strings can be returned, which then serve up the page
    return "<div>This is a second page</div>"

# pages may have multiple routes
@app.route("/page3")
@app.route("/pagetrois")
def page3():
    return "<div>This is a third page</div>"

# All routes by default only accept GET requests. More can be specified using method or methods as a parameter
# and passing in a string or list
@app.route("/data", methods=['GET', 'PUT'])
def data():
    # check if the request is a PUT request
    if request.method == 'PUT':
        payload = request.get_json()
        for key, value in payload.items():
            config[key] = value
        # PUT requests do not traditionally return anything, but this can be returned if you wish to have a response message
        return ""
    else:
        # if the request is a GET, then the config object is returned. This is interpreted by the web browser as a JSON object
        return config

